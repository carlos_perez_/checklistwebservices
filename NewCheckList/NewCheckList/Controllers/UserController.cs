﻿using NewCheckList.Globals;
using NewCheckList.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewCheckList.Controllers
{
    public class UserController : ApiController
    {
        ConnectionInfo info = new ConnectionInfo();
        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter da;
        DataSet ds;

        public UserController()
        {
            connection = new SqlConnection(info.getConnectionString());
        }

        [HttpGet]
        public UserOverhead ListUsers()
        {
            UserOverhead model = new UserOverhead();

            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand("p_getUsers", connection);
            command.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = command;
            da.Fill(ds);
            
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                OldUserModel mod = new OldUserModel();
                //mod.Code = Convert.ToInt32(row["user_code"]);
                mod.Code = row["user_name"].ToString();
                mod.Email = row["email"].ToString();
                mod.ID = Convert.ToInt32(row["user_id"]);
                mod.Password = row["current_password"].ToString();
                mod.RoleID = Convert.ToInt32(row["user_role_id"]);
                mod.Username = row["name"].ToString();
                model.users.Add(mod);
            }

            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand("p_getUserGroup", connection);
            command.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = command;
            da.Fill(ds);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                OldUserGroup mod = new OldUserGroup();
                mod.GroupID = Convert.ToInt32(row["group_id"]);
                mod.UserID = Convert.ToInt32(row["user_id"]);
                model.userGroups.Add(mod);
            }
            return model;
        }   
    }
}
