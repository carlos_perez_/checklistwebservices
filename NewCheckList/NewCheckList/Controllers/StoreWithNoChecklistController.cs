﻿using NewCheckList.Globals;
using NewCheckList.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewCheckList.Controllers
{
    public class StoreWithNoChecklistController : ApiController
    {
        ConnectionInfo info = new ConnectionInfo();
        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter da;
        DataSet ds;
        CompletedChecklistServiceController _checkListService = new CompletedChecklistServiceController();
        UsersController _userService = new UsersController();
        StoreController _storeService = new StoreController();

        public StoreWithNoChecklistController()
        {
            connection = new SqlConnection(info.getConnectionString());
        }

        public List<Result> Get(int id, DateTime from, DateTime to)
        {
            List<Result> results = new List<Result>();
            string query = "select * from store where store_number = @storeNumber";
            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@storeNumber", id);
            da.SelectCommand = command;
            da.Fill(ds);

            StoreModel store = new StoreModel();
            if (ds.Tables[0].Rows.Count > 0)
            {
                store.storeName = ds.Tables[0].Rows[0]["store_name"].ToString();
                store.storeNumber = Convert.ToInt16(ds.Tables[0].Rows[0]["store_number"]);
            }
            else
            {
                return results;
            }

            query = "select * from checkList where scheduleType = @type and isActive = 1";
            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@type", "Daily");
            da.SelectCommand = command;
            da.Fill(ds);

            List<ReportChecklist> list = new List<ReportChecklist>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ReportChecklist c = new ReportChecklist();
                c.Id = Convert.ToInt16(row["ID"]);
                c.Name = row["Name"].ToString();
                c.CreateDate = Convert.ToDateTime(row["CreateDate"]);
                c.IsActive = Convert.ToBoolean(row["IsActive"]);
                c.ScheduleType = row["ScheduleType"].ToString();
                c.LastModifiedDate = Convert.ToDateTime(row["LastModifiedDate"]);
                string temp = row["TimeDue"].ToString();
                c.TimeDue = TimeSpan.Parse(temp);
                list.Add(c);
            }

            List<CompletedChecklist> completedChecklist = _checkListService.getStoreList(id, from, to);
            List<Result> result = new List<Result>();
            foreach (CompletedChecklist ck in completedChecklist)
            {
                for (DateTime dt = from.Date; dt <= to; dt = dt.AddDays(1))
                {
                    if (!completedChecklist.Where(a => (a.checklistID == ck.ID && a.SubmittedTime.Date.Equals(dt))).Any())
                    {
                        result.Add(new Result() { ChecklistName = ck.Name, StoreNumber = store.storeNumber, StoreName = store.storeName, Date = dt.ToString("d") });
                    }
                }
            }
            return result;
        }

        public List<Result> GetByUser(string usercode, DateTime from, DateTime to)
        {
            List<Result> results = new List<Result>();

            UsersModel user = _userService.GetUserByCode(usercode);
            if (user == null)
            {
                return results;
            }

            List<StoreModel> stores = _storeService.GetStores(user.userID).ToList();
            if (stores.Count == 0)
            {
                return results;
            }

            foreach (StoreModel store in stores)
            {
                results.AddRange(Get(store.storeNumber, from, to));
            }
            return results;
        }

        public class Result
        {
            public string ChecklistName { get; set; }
            public int StoreNumber { get; set; }
            public string StoreName { get; set; }
            public string Date { get; set; }
        }
    }
}
