﻿using NewCheckList.Globals;
using NewCheckList.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewCheckList.Controllers
{
    public class StoreChecklistController : ApiController
    {
        ConnectionInfo info = new ConnectionInfo();
        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter da;
        DataSet ds;
        UsersController _userService = new UsersController();
        StoreController _storeService = new StoreController();

        public StoreChecklistController()
        {
            connection = new SqlConnection(info.getConnectionString());
        }

        //public List<StoreChecklistVM> Get(int id, DateTime from, DateTime to)
        //{
        //    List<int> storeNumbers = new List<int>();
        //    storeNumbers.Add(id);
        //    return Get(storeNumbers, from, to);
        //}

        //private List<StoreChecklistVM> Get(List<int> storeNumbers, DateTime from, DateTime to)
        //{

        //    string query = "select * from checkList where scheduleType = @type and isActive = 1";
        //    da = new SqlDataAdapter();
        //    ds = new DataSet();
        //    command = new SqlCommand(query, connection);
        //    command.Parameters.AddWithValue("@type", "Daily");
        //    da.SelectCommand = command;
        //    da.Fill(ds);
        //    List<ReportChecklist> checkLists = new List<ReportChecklist>();

        //    foreach (DataRow row in ds.Tables[0].Rows)
        //    {
        //        ReportChecklist c = new ReportChecklist();
        //        c.Id = Convert.ToInt16(row["ID"]);
        //        c.Name = row["Name"].ToString();
        //        c.CreateDate = Convert.ToDateTime(row["CreateDate"]);
        //        c.IsActive = Convert.ToBoolean(row["IsActive"]);
        //        c.ScheduleType = row["ScheduleType"].ToString();
        //        c.LastModifiedDate = Convert.ToDateTime(row["LastModifiedDate"]);
        //        string temp = row["TimeDue"].ToString();
        //        c.TimeDue = TimeSpan.Parse(temp);
        //        checkLists.Add(c);
        //    }

        //    List<StoreChecklistVM> list = checkLists.Select(a => new StoreChecklistVM()
        //    {
        //        Name = a.Name,
        //        ChecklistID = a.Id
        //    }).ToList();


        //    //var storevar = _storeService.Where(a => storeNumbers.Contains(a.Number)).ToList();

        //    query = "select * from store where ";
        //    bool con = false;
        //    foreach (int i in storeNumbers)
        //    {
        //        if (con)
        //        {
        //            query += " or ";
        //        }
        //        query += "store_number = " + i;
        //        con = true;
        //    }

        //    da = new SqlDataAdapter();
        //    ds = new DataSet();
        //    command = new SqlCommand(query, connection);
        //    da.SelectCommand = command;
        //    try
        //    {
        //        da.Fill(ds);
        //    }
        //    catch
        //    {

        //    }

        //    List<int> storeID = new List<int>();

        //    foreach (DataRow row in ds.Tables[0].Rows)
        //    {
        //        storeID.Add(Convert.ToInt32(row["store_number"]));
        //    }

        //    List<CheckListGroupModel> checklistGroup = new List<CheckListGroupModel>();

        //    query = "select cg.Id, cg.GroupID, cg.ChecklistID from ChecklistGroup cg inner join Checklist cl on cl.ID = cg.ChecklistID";
        //    da = new SqlDataAdapter();
        //    ds = new DataSet();
        //    command = new SqlCommand(query, connection);
        //    try
        //    {
        //        connection.Open();
        //        command.ExecuteNonQuery();
        //    }
        //    catch(Exception e)
        //    {
        //        string mes = e.Message;
        //    }
        //    finally
        //    {
        //        connection.Close();
        //    }

        //    foreach (DataRow row in ds.Tables[0].Rows)
        //    {
        //        CheckListGroupModel mod = new CheckListGroupModel();
        //        mod.id = Convert.ToInt32(row["Id"]);
        //        mod.GroupID = Convert.ToInt32(row["group_id"]);
        //        mod.checkListID = Convert.ToInt32(row["ChecklistID"]);
        //        checklistGroup.Add(mod);
        //    }
            

        //    foreach (DataRow row in ds.Tables[0].Rows)
        //    {
        //        CompletedChecklist c = new CompletedChecklist();
        //        c.StoreID = Convert.ToInt16(row["StoreID"]);
        //        c.Name = row["Name"].ToString();
        //        c.SubmittedTime = Convert.ToDateTime(row["SubmittedTime"]);
        //        c.checklistID = Convert.ToInt16(row["ChecklistID"]);
                
        //    }            

        //    List<CompletedChecklist> completedChecklist = _completedChecklistService.Get(storeNumbers, from, to).ToList();
        //    list.ForEach(a =>
        //    {
        //        a.NumOfRequired = ((to - from).Days + 1) * storeNumbers.Count();
        //        var completedLists = completedChecklist.Where(c => (c.CheckListID == a.ChecklistID && c.IsComplete == true));
        //        if (completedLists.Any())
        //        {
        //            a.NumOfCompleted = completedLists.Count();
        //            var avgtime = completedLists.Select(c => (c.SubmittedTime - c.StartTime).TotalSeconds);
        //            a.AverageSecondsToComplete = (int)avgtime.Average();
        //            var ontime = completedLists.Where(c =>
        //            {
        //                return c.SubmittedTime < c.StartTime.Date.Add(checkLists.First(cl => cl.Id == c.CheckListID).TimeDue);
        //            }).Count();
        //            a.PercentOfOnTime = (decimal)ontime / a.NumOfCompleted;
        //        }
        //    });
        //    return list;
        //}


    }
}
