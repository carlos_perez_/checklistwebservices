﻿using NewCheckList.Globals;
using NewCheckList.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewCheckList.Controllers
{
    public class UsersController : ApiController
    {
        ConnectionInfo info = new ConnectionInfo();
        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter da;
        DataSet ds;

        public UsersController()
        {
            connection = new SqlConnection(info.getConnectionString());
        }

        [HttpGet]
        public List<UsersModel> GetUsers()
        {
            List<UsersModel> users = new List<UsersModel>();
            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand("p_getUsers", connection);
            command.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = command;
            da.Fill(ds);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                UsersModel user = new UsersModel();
                user.userID = Convert.ToInt16(row["user_id"]);
                user.userName = row["user_name"].ToString();
                user.password = row["current_password"].ToString();
                user.email = row["email"].ToString();
                user.userRoleID = Convert.ToInt16(row["user_role_id"]);
                //user.code = Convert.ToInt16(row["user_code"]);
                user.code = row["user_code"].ToString();
                users.Add(user);
            }

            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand("p_getUserGroup", connection);
            command.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = command;
            da.Fill(ds);
            int currentIndex = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                int userID = Convert.ToInt16(row["user_id"]);
                bool found = false;
                while (!found)
                {
                    if (currentIndex < users.Count)
                    {
                        if (users[currentIndex].userID == userID)
                        {
                            UserGroupsModel userGroup = new UserGroupsModel();
                            userGroup.groupID = Convert.ToInt16(row["group_id"]);
                            users[currentIndex].userGroups.Add(userGroup);
                            found = true;
                        }
                        else
                        {
                            if (users[currentIndex].userID > userID)
                            {
                                break;
                            }
                            else
                            {
                                currentIndex++;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return users;
        }

        public UsersModel GetUserByCode(string code)
        {
            UsersModel mod = new UsersModel();
            da = new SqlDataAdapter();
            ds = new DataSet();
            string query = "select * from [User] where user_code = @code";
            command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@code", code);
            da.SelectCommand = command;
            da.Fill(ds);

            mod.userID = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"]);
            mod.userName = ds.Tables[0].Rows[0]["user_name"].ToString();
            mod.email = ds.Tables[0].Rows[0]["email"].ToString();
            mod.userRoleID = Convert.ToInt16(ds.Tables[0].Rows[0]["user_role_id"]);
            mod.code = code;

            return mod;
        }

    }
}
