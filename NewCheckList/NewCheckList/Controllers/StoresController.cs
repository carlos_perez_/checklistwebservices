﻿using NewCheckList.Globals;
using NewCheckList.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewCheckList.Controllers
{
    public class StoresController : ApiController
    {
        ConnectionInfo info = new ConnectionInfo();
        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter da;
        DataSet ds;

        public StoresController()
        {
            connection = new SqlConnection(info.getConnectionString());
        }

        public List<StoreModel> GetStores()
        {
            List<StoreModel> stores = new List<StoreModel>();
            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand("p_getStores", connection);
            command.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = command;
            da.Fill(ds);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                StoreModel mod = new StoreModel();
                mod.storeID = Convert.ToInt16(row["store_id"]);
                mod.storeNumber = Convert.ToInt16(row["store_number"]);
                mod.storeName = row["store_name"].ToString();
                stores.Add(mod);
            }

            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand("p_getStoreGroup", connection);
            command.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = command;
            da.Fill(ds);

            int currentIndex = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                int storeID = Convert.ToInt16(row["store_id"]);
                bool found = false;
                while (!found)
                {
                    if (currentIndex <= stores.Count)
                    {
                        if (stores[currentIndex].storeID == storeID)
                        {
                            StoreGroupModel storeGroup = new StoreGroupModel();
                            storeGroup.groupID = Convert.ToInt16(row["group_id"]);
                            storeGroup.storeID = Convert.ToInt16(row["store_id"]);
                            storeGroup.id = Convert.ToInt16(row["id"]);
                            stores[currentIndex].storeGroups.Add(storeGroup);
                            found = true;
                        }
                        else
                        {
                            if (stores[currentIndex].storeID > storeID)
                            {
                                break;
                            }
                            else
                            {
                                currentIndex++;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return stores;
        }
    }
}
