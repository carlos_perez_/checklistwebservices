﻿using NewCheckList.Globals;
using NewCheckList.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewCheckList.Controllers
{
    public class StoreController : ApiController
    {
        ConnectionInfo info = new ConnectionInfo();
        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter da;
        DataSet ds;

        public StoreController()
        {
            connection = new SqlConnection(info.getConnectionString());
        }

        [HttpGet]
        public OverHeadStoreModel ListStores()
        {
            OverHeadStoreModel model = new OverHeadStoreModel();
            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand("p_getStores", connection);
            command.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = command;
            da.Fill(ds);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                OldStoreModel mod = new OldStoreModel();
                mod.ID = Convert.ToInt16(row["store_id"]);
                mod.Name = row["store_name"].ToString();
                mod.Number = Convert.ToInt16(row["store_number"]);
                model.stores.Add(mod);
            }

            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand("p_getStoreGroup", connection);
            command.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = command;
            da.Fill(ds);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                OldStoreGroupModel mod = new OldStoreGroupModel();
                mod.GroupID = Convert.ToInt16(row["group_id"]);
                mod.StoreID = Convert.ToInt16(row["store_id"]);
                model.storeGroups.Add(mod);
            }

            return model;
        }


        public List<StoreModel> GetStores(int userID)
        {
            List<StoreModel> stores = new List<StoreModel>();
            da = new SqlDataAdapter();
            ds = new DataSet();
            string query = "select distinct * from store inner join StoreGroup sg on store.store_id = sg.store_id inner join UserGroup ug on sg.group_id = ug.group_id where ug.user_id = @userID";
            command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@userID", userID);
            da.SelectCommand = command;
            da.Fill(ds);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                StoreModel mod = new StoreModel();
                mod.storeName = row["store_name"].ToString();
                mod.storeNumber = Convert.ToInt16(row["store_number"]);
                stores.Add(mod);
            }

            return stores;
        }
    }
}
