﻿using NewCheckList.Globals;
using NewCheckList.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewCheckList.Controllers
{
    public class CheckListController : ApiController
    {
        ConnectionInfo info = new ConnectionInfo();
        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter da;
        DataSet ds;

        public CheckListController()
        {
            connection = new SqlConnection(info.getConnectionString());
        }

        [HttpGet]
        public OverHeadCheckList ListChecklist()
        {
            OverHeadCheckList model = new OverHeadCheckList();
            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand("p_getCheckLists", connection);
            command.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = command;
            da.Fill(ds);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                OldChecklistModel checkList = new OldChecklistModel();
                checkList.ID = Convert.ToInt16(row["ID"]);
                checkList.Name = row["Name"].ToString();
                checkList.CreateDate = Convert.ToDateTime(row["CreateDate"]);
                checkList.IsActive = Convert.ToInt16(row["IsActive"]);
                checkList.LastModifiedDate = Convert.ToDateTime(row["lastModifiedDate"]);
                model.checkLists.Add(checkList);
            }

            #region checkListHeader
            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand("p_getCheckListheader", connection);
            command.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = command;
            da.Fill(ds);


            int currentIndex = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                int checkListID = Convert.ToInt16(row["ChecklistID"]);
                bool found = false;
                while (!found)
                {
                    if (currentIndex < model.checkLists.Count)
                    {
                        if (model.checkLists[currentIndex].ID == checkListID)
                        {
                            OldChecklistHeader mod = new OldChecklistHeader();
                            mod.ID = Convert.ToInt16(row["ID"]);
                            mod.ChecklistID = checkListID;
                            mod.ElementType = Convert.ToInt16(row["ElementType"]);
                            mod.ElementData = row["ElementData"].ToString();
                            mod.Width = Convert.ToInt16(row["width"]);
                            mod.X = Convert.ToInt16(row["x"]);
                            model.checkLists[currentIndex].Headers.Add(mod);
                            found = true;
                        }
                        else
                        {
                            if (model.checkLists[currentIndex].ID > checkListID)
                            {
                                break;
                            }
                            else
                            {
                                currentIndex++;
                            }

                        }
                    }
                    else
                    {
                        break;
                    }
                }

            }
            #endregion

            #region check list row
            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand("p_getChecklistRow", connection);
            command.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = command;
            da.Fill(ds);

            currentIndex = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                int checkListID = Convert.ToInt16(row["CheckListID"]);
                bool found = false;

                while (!found)
                {
                    if (currentIndex < model.checkLists.Count)
                    {
                        if (model.checkLists[currentIndex].ID == checkListID)
                        {
                            OldCheckListRow r = new OldCheckListRow();
                            r.ID = Convert.ToInt16(row["ID"]);
                            r.ChecklistID = Convert.ToInt16(row["CheckListID"]);
                            r.OrderNumber = Convert.ToInt16(row["OrderNumber"]);
                            //r.photoRequired = Convert.ToInt16(row["photoRequired"]);
                            model.checkLists[currentIndex].Rows.Add(r);
                            found = true;
                        }
                        else
                        {
                            if (model.checkLists[currentIndex].ID > checkListID)
                            {
                                break;
                            }
                            else
                            {
                                currentIndex++;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }

            }
            #endregion

            #region CheckListElement
            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand("p_getCheckListElement", connection);
            command.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = command;
            da.Fill(ds);

            List<OldCheckListElements> elements = new List<OldCheckListElements>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                OldCheckListElements mod = new OldCheckListElements();
                mod.ID = Convert.ToInt16(row["ID"]);
                mod.RowID = Convert.ToInt16(row["RowID"]);
                mod.Type = Convert.ToInt16(row["Type"]);
                mod.Data = row["Data"].ToString();
                mod.Width = Convert.ToInt16(row["Width"]);
                mod.Height = Convert.ToInt16(row["Height"]);
                mod.X = Convert.ToInt16(row["x"]);
                elements.Add(mod);
            }

            foreach (OldChecklistModel c in model.checkLists)
            {
                foreach (OldCheckListRow r in c.Rows)
                {

                    int index = 0;
                    bool found = false;
                    int foundIndex = 0;
                    while (true)
                    {
                        if (!found)
                        {
                            if (index < elements.Count)
                            {
                                if (r.ID == elements[index].RowID)
                                {
                                    foundIndex = index;
                                    found = true;
                                }
                                else
                                {
                                    index++;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            if (foundIndex < elements.Count)
                            {
                                if (elements[foundIndex].RowID == r.ID)
                                {
                                    r.Elements.Add(elements[foundIndex]);
                                    elements.RemoveAt(foundIndex);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
            #endregion

            #region Check List Group
            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand("p_getCheckListGroup", connection);
            command.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = command;
            da.Fill(ds);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                OldCheckListGroup mod = new OldCheckListGroup();
                mod.CheckListID = Convert.ToInt16(row["ChecklistID"]);
                mod.GroupID = Convert.ToInt16(row["GroupID"]);
                model.Groups.Add(mod);
            }

            #endregion
            return model;
        }

        [HttpPost]
        public HttpResponseMessage SubmitCompletedChecklist(CompletedChecklist value)
        {
            Int32 completedChecklistID = 0;
            string query = "INSERT INTO CompletedChecklist (ChecklistID, StoreID, UserID, StoreManager, Name, StartTime, SubmittedTime, IsComplete, Signature, maxScore, totalScore) OUTPUT INSERTED.ID VALUES (@ChecklistID, @StoreID, @UserID, @StoreManager, @Name, @StartTime, @SubmittedTime, @IsComplete, @Signature, @maxScore, @totalScore)";
            //string query = "INSERT INTO CompletedChecklist (ChecklistID, StoreID, UserID, StoreManager, Name, StartTime, SubmittedTime, IsComplete, Signature) OUTPUT INSERTED.ID VALUES (@ChecklistID, @StoreID, @UserID, @StoreManager, @Name, @StartTime, @SubmittedTime, @IsComplete, @Signature)";
            command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@checkListID", value.ID);
            command.Parameters.AddWithValue("@StoreID", value.StoreID);
            command.Parameters.AddWithValue("@UserID", value.UserID);
            command.Parameters.AddWithValue("@StoreManager", value.StoreManager);
            command.Parameters.AddWithValue("@Name", value.Name);
            command.Parameters.AddWithValue("@StartTime", value.StartTime);
            command.Parameters.AddWithValue("@SubmittedTime", value.SubmittedTime);
            command.Parameters.AddWithValue("@IsComplete", value.IsComplete);
            command.Parameters.AddWithValue("@Signature", value.Signature);
            command.Parameters.AddWithValue("@maxScore", value.maxScore);
            command.Parameters.AddWithValue("@totalScore", value.totalScore);
            try
            {
                connection.Open();
                completedChecklistID = (Int32)command.ExecuteScalar();
            }
            catch (Exception e)
            {
                //return e.Message;
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            finally
            {
                connection.Close();
            }

            DataTable table = new DataTable();
            int columnNum = 6;

            for (int i = 0; i < columnNum; ++i)
            {
                table.Columns.Add();
            }

            int rowCount = 0;

            foreach (ChecklistHeader header in value.Headers)
            {
                table.Rows.Add();
                table.Rows[rowCount][1] = completedChecklistID;
                table.Rows[rowCount][2] = header.ElementType;
                table.Rows[rowCount][3] = header.ElementData;
                table.Rows[rowCount][4] = header.Width;
                table.Rows[rowCount][5] = header.X;
                rowCount++;
            }
            writeToTable(table, columnNum, "CompletedChecklistHeader");

            DataTable elementTable = new DataTable();
            columnNum = 8;
            rowCount = 0;
            for (int i = 0; i < columnNum; ++i)
            {
                elementTable.Columns.Add();
            }
            foreach (CompletedChecklistRow row in value.Rows)
            {
                Int32 newRowID = 0;
                query = "INSERT INTO CompletedChecklistRow (CompletedChecklistID, OrderNumber, Image1, Image2, Note) OUTPUT INSERTED.ID VALUES (@CompletedChecklistID, @OrderNumber, @Image1, @Image2, @Note)";
                command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter() { ParameterName = "@CompletedChecklistID", Value = completedChecklistID });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@OrderNumber", Value = row.OrderNumber });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@Image1", Value = row.Image1 });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@Image2", Value = row.Image2 });
                command.Parameters.Add(new SqlParameter() { ParameterName = "@Note", Value = row.Note });
                try
                {
                    connection.Open();
                    newRowID = (Int32)command.ExecuteScalar();
                }
                catch (Exception e)
                {
                    //return e.Message;
                }
                finally
                {
                    connection.Close();
                }
                if (newRowID != 0)
                {
                    foreach (CompletedChecklistElement element in row.Elements)
                    {
                        elementTable.Rows.Add();
                        elementTable.Rows[rowCount][1] = newRowID;
                        elementTable.Rows[rowCount][2] = element.Type;
                        elementTable.Rows[rowCount][3] = element.Data;
                        elementTable.Rows[rowCount][4] = element.Width;
                        elementTable.Rows[rowCount][5] = element.Height;
                        elementTable.Rows[rowCount][6] = element.X;
                        if (element.Result != null)
                        {
                            elementTable.Rows[rowCount][7] = element.Result;
                        }
                        else
                        {
                            elementTable.Rows[rowCount][7] = DBNull.Value;
                        }

                        rowCount++;
                    }
                }
            }

            writeToTable(elementTable, columnNum, "CompletedChecklistElement");
            return Request.CreateResponse(HttpStatusCode.OK);
            //return "good";
        }

        public int writeToTable(DataTable table, int colNum, string tableName)
        {
            SqlBulkCopy bulkcopy = new SqlBulkCopy(connection);
            connection.Open();
            try
            {
                for (int i = 1; i < colNum; ++i)
                {
                    bulkcopy.ColumnMappings.Add(i, i);
                }
                bulkcopy.DestinationTableName = tableName;
                bulkcopy.WriteToServer(table);
            }
            catch
            {
                connection.Close();
                return 100;
            }
            finally
            {
                connection.Close();
            }
            return 200;
        }
    }
}
