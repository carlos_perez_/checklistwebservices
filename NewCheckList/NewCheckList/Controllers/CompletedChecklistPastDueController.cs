﻿using NewCheckList.Globals;
using NewCheckList.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewCheckList.Controllers
{
    public class CompletedChecklistPastDueController : ApiController
    {
        ConnectionInfo info = new ConnectionInfo();
        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter da;
        DataSet ds;
        UsersController _userService = new UsersController();
        StoreController _storeService = new StoreController();

        public CompletedChecklistPastDueController()
        {
            connection = new SqlConnection(info.getConnectionString());
        }

        public List<CompletedChecklistPastDue> Get(int id,DateTime from,DateTime to)
        {
            List<CompletedChecklistPastDue> completedChecklist = new List<CompletedChecklistPastDue>();
            int storeNumber = 0;
            string query = "select * from store where store_number = @storeNumber";
            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@storeNumber", id);
            da.SelectCommand = command;
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                storeNumber = Convert.ToInt16(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return completedChecklist;
            }

            query = "select * from checkList where scheduleType = @type and isActive = 1";
            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@type", "Daily");
            da.SelectCommand = command;
            da.Fill(ds);
            List<ReportChecklist> list = new List<ReportChecklist>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ReportChecklist c = new ReportChecklist();
                c.Id = Convert.ToInt16(row["ID"]);
                c.Name = row["Name"].ToString();
                c.CreateDate = Convert.ToDateTime(row["CreateDate"]);
                c.IsActive = Convert.ToBoolean(row["IsActive"]);
                c.ScheduleType = row["ScheduleType"].ToString();
                c.LastModifiedDate = Convert.ToDateTime(row["LastModifiedDate"]);
                string temp = row["TimeDue"].ToString();
                c.TimeDue = TimeSpan.Parse(temp);
                list.Add(c);
            }
            var checklistID = list.Select(a => a.Id);
            query = "select * from completedChecklist where ";
            bool con = false;
            foreach (int i in checklistID)
            {
                if (con)
                {
                    query += " or ";
                }
                query += "checklistID = " + i;
                con = true;
            }

            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand(query, connection);
            da.SelectCommand = command;
            try
            {
                da.Fill(ds);
            }
            catch
            {
                return completedChecklist;
            }

            foreach(DataRow row in ds.Tables[0].Rows)
            {
                CompletedChecklist c = new CompletedChecklist();
                c.StoreID = Convert.ToInt16(row["StoreID"]);
                c.Name = row["Name"].ToString();
                c.SubmittedTime = Convert.ToDateTime(row["SubmittedTime"]);
                c.checklistID = Convert.ToInt16(row["ChecklistID"]);
                bool passed = IsCompletedPassDue(c, list.Where(a => a.Id == c.checklistID).FirstOrDefault().TimeDue);
                if (passed)
                {
                    completedChecklist.Add(new CompletedChecklistPastDue() { StoreNumber = c.StoreID, ChecklistName = c.Name, TimeDue = list.Where(a => a.Id == c.checklistID).FirstOrDefault().TimeDue, TimeSubmitted = c.SubmittedTime });
                }
            }
            return completedChecklist;
        }

        public List<CompletedChecklistPastDue> GetStoresByUser(string userCode, DateTime from, DateTime to)
        {

            List<CompletedChecklistPastDue> results = new List<CompletedChecklistPastDue>();

            UsersModel user = _userService.GetUserByCode(userCode);
            if (user == null)
            {
                return results;
            }

            List<StoreModel> stores = _storeService.GetStores(user.userID);
            if (stores.Count == 0)
            {
                return results;
            }

            foreach (var store in stores)
            {
                results.AddRange(Get(store.storeNumber, from, to));
            }
            return results;
        }

        [HttpHead]
        public bool IsCompletedPassDue(CompletedChecklist completedChecklist, TimeSpan timeDue)
        {
            DateTime completeTime = completedChecklist.SubmittedTime;
            //DateTime startTime = completedChecklist.StartTime;
            //DateTime dueTime = startTime.Date.Add(timeDue);
            DateTime dueTime = completeTime.Date.Add(timeDue);
            return completeTime > dueTime;
        }
    }
}