﻿using NewCheckList.Globals;
using NewCheckList.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewCheckList.Controllers
{
    public class CompletedChecklistServiceController : ApiController
    {
        ConnectionInfo info = new ConnectionInfo();
        SqlConnection connection;
        SqlCommand command;
        SqlDataAdapter da;
        DataSet ds;

        public CompletedChecklistServiceController()
        {
            connection = new SqlConnection(info.getConnectionString());
        }

        public List<CompletedChecklist> getStoreList(int storeNumber, DateTime from, DateTime to)
        {
            List<CompletedChecklist> list = new List<CompletedChecklist>();
            string f = from.ToShortDateString();
            string t = to.ToShortDateString();
            string query = "select * from completedChecklist where StoreID = " + storeNumber.ToString() + " and SubmittedTime >= '" + f + "' and  SubmittedTime <= '" + t + "'";
            da = new SqlDataAdapter();
            ds = new DataSet();
            command = new SqlCommand(query, connection);
            da.SelectCommand = command;
            da.Fill(ds);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                CompletedChecklist c = new CompletedChecklist();
                c.Name = row["Name"].ToString();
                c.checklistID = Convert.ToInt16(row["ChecklistID"]);
                c.StoreID = Convert.ToInt16(row["StoreID"]);
                c.StartTime = Convert.ToDateTime(row["StartTime"]);
                c.SubmittedTime = Convert.ToDateTime(row["SubmittedTime"]);
                c.IsComplete = Convert.ToBoolean(row["IsComplete"]);
                c.StoreManager = row["StoreManager"].ToString();
                list.Add(c);
            }

            return list;
        }
    }
}
