﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class OldChecklistModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public int IsActive { get; set; }
        public List<OldChecklistHeader> Headers { get; set; }
        public List<OldCheckListRow> Rows { get; set; }

        public OldChecklistModel()
        {
            Headers = new List<OldChecklistHeader>();
            Rows = new List<OldCheckListRow>();
        }
        
    }
}