﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class OverHeadCheckList
    {
        public List<OldChecklistModel> checkLists { get; set; }
        public List<OldCheckListGroup> Groups { get; set; }

        public OverHeadCheckList()
        {
            checkLists = new List<OldChecklistModel>();
            Groups = new List<OldCheckListGroup>();
        }
    }
}