﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class ReportChecklist
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public Boolean IsActive { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string ScheduleType { get; set; }
        public TimeSpan TimeDue { get; set; }
    }
}