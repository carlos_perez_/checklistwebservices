﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class CompletedChecklistRow : ChecklistRow
    {
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Note { get; set; }
        public new List<CompletedChecklistElement> Elements { get; set; }
    }
}