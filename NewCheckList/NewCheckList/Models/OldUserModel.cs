﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class OldUserModel
    {
        public string Code { get; set; }
        public string Email { get; set; }
        public int ID { get; set; }
        public string Password { get; set; }
        public int RoleID { get; set; }
        public string Username { get; set; }
    }
}