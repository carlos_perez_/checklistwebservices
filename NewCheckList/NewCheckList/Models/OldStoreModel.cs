﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class OldStoreModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
    }
}