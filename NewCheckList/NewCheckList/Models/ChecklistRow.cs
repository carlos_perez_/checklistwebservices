﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class ChecklistRow
    {
        public int ID { get; set; }
        public int ChecklistID { get; set; }
        public int OrderNumber { get; set; }
        public List<ChecklistElement> Elements { get; set; }
    }
}