﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class OldChecklistHeader
    {
        public int ChecklistID { get; set; }
        public string ElementData { get; set; }
        public int ElementType { get; set; }
        public int ID { get; set; }
        public int Width { get; set; }
        public int X { get; set; }
    }
}