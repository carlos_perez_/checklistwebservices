﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class CompletedChecklist : Checklist
    {
        int Id { get; set; }
        public int StoreID { get; set; }
        public int UserID { get; set; }
        public string StoreManager { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime SubmittedTime { get; set; }
        public bool IsComplete { get; set; }
        public new List<CompletedChecklistRow> Rows { get; set; }
        public string Signature { get; set; }
        public int maxScore { get; set; }
        public int totalScore { get; set; }
        public int checklistID { get; set; }
    }
}