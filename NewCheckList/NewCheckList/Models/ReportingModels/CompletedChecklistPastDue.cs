﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class CompletedChecklistPastDue
    {
        public int StoreNumber { get; set; }
        public string ChecklistName { get; set; }
        public TimeSpan TimeDue { get; set; }
        public DateTime TimeSubmitted { get; set; }
    }
}