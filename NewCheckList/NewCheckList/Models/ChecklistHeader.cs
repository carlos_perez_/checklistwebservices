﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class ChecklistHeader
    {
        public int ID { get; set; }
        public int ChecklistID { get; set; }
        public int ElementType { get; set; }
        public string ElementData { get; set; }
        public double Width { get; set; }
        public double X { get; set; }
    }
}