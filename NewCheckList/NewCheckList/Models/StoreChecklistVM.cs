﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class StoreChecklistVM
    {
        public int ChecklistID { get; set; }
        public string Name { get; set; }
        public int NumOfRequired { get; set; }
        public int NumOfCompleted { get; set; }
        public int AverageSecondsToComplete { get; set; }
        public decimal PercentOfOnTime { get; set; }
    }
}