﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class OverHeadStoreModel
    {
        public List<OldStoreGroupModel> storeGroups { get; set; }
        public List<OldStoreModel> stores { get; set; }

        public OverHeadStoreModel()
        {
            storeGroups = new List<OldStoreGroupModel>();
            stores = new List<OldStoreModel>();
        }
    }
}