﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class OldStoreGroupModel
    {
        public int GroupID { get; set; }
        public int StoreID { get; set; }
    }
}