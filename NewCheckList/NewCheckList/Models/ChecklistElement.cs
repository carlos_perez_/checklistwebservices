﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class ChecklistElement
    {
        public int ID { get; set; }
        public int RowID { get; set; }
        public int Type { get; set; }
        public string Data { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double X { get; set; }
    }
}