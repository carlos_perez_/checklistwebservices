﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class UserOverhead
    {
        public List<OldUserModel> users { get; set; }
        public List<OldUserGroup> userGroups { get; set; }

        public UserOverhead()
        {
            users = new List<OldUserModel>();
            userGroups = new List<OldUserGroup>();
        }
    }
}