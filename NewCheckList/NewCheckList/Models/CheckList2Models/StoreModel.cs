﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class StoreModel
    {
        public int storeID { get; set; }
        public int storeNumber { get; set; }
        public string storeName { get; set; }
        public List<StoreGroupModel> storeGroups { get; set; }

        public StoreModel()
        {
            storeGroups = new List<StoreGroupModel>();
        }
    }
}