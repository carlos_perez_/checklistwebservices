﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class StoreGroupModel
    {
        public int storeID { get; set; }
        public int groupID { get; set; }
        public int id { get; set; }
    }
}