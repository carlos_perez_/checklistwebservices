﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class ChecklistHeaderModel
    {
        public int id { get; set; }
        public int checkListID { get; set; }
        public int elementType { get; set; }
        public string elementData { get; set; }
        public int width { get; set; }
        public int x { get; set; }
    }
}