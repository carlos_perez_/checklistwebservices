﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class ChecklistRowModel
    {
        public int id { get; set; }
        public int checkListID { get; set; }
        public int order { get; set; }
        public List<CheckListElementModel> elements { get; set; }

        public ChecklistRowModel()
        {
            elements = new List<CheckListElementModel>();
        }
    }
}