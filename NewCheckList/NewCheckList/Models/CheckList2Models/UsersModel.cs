﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class UsersModel
    {
        public int userID { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public int userRoleID { get; set; }
        public string code { get; set; }
        public List<UserGroupsModel> userGroups { get; set; }

        public UsersModel()
        {
            userGroups = new List<UserGroupsModel>();
        }
    }
}