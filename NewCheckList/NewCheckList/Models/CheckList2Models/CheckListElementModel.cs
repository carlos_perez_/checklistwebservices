﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class CheckListElementModel
    {
        public int id { get; set; }
        public int rowID { get; set; }
        public int type { get; set; }
        public string data { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public int x { get; set; }
    }
}