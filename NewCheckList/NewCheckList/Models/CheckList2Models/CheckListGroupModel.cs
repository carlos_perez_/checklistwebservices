﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class CheckListGroupModel
    {
        public int id { get; set; }
        public int checkListID { get; set; }
        public int GroupID { get; set; }
    }
}