﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class CheckListModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime createDate { get; set; }
        public DateTime lastModifiedDate { get; set; }
        public int isActive { get; set; }
        public List<ChecklistHeaderModel> headers { get; set; }
        public List<ChecklistRowModel> rows { get; set; }
        public List<CheckListGroupModel> groups { get; set; }

        public CheckListModel()
        {
            headers = new List<ChecklistHeaderModel>();
            rows = new List<ChecklistRowModel>();
            groups = new List<CheckListGroupModel>();
        }
    }
}