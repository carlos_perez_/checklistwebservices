﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class OldCheckListRow
    {
        public int ChecklistID { get; set; }
        public int ID { get; set; }
        public int OrderNumber { get; set; }
        public int photoRequired { get; set; }
        public List<OldCheckListElements> Elements { get; set; }

        public OldCheckListRow()
        {
            Elements = new List<OldCheckListElements>();
        }
    }
}