﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class OldCheckListElements
    {
        public string Data { get; set; }
        public int Height { get; set; }
        public int ID { get; set; }
        public int RowID { get; set; }
        public int Type { get; set; }
        public int Width { get; set; }
        public int X { get; set; }
    }
}