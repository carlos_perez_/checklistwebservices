﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewCheckList.Models
{
    public class CompletedChecklistElement : ChecklistElement
    {
        public string Result { get; set; }
    }
}